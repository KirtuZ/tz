<?php

namespace KirtuZ\MathBig;

require_once __DIR__ . '/../../src/autoload.php';

use PHPUnit\Framework\TestCase;
use KirtuZ\MathBig\Operations\OperationSum;
use KirtuZ\MathBig\Operations\OperationInterface;

class MathBigTest extends TestCase
{

    /**
     * @var MathBig
     */
    protected $object;

    protected function setUp()
    {
        
    }

    protected function tearDown()
    {
        
    }

    /**
     * @covers KirtuZ\MathBig\MathBig::process
     */
    public function testProcessFloat()
    {
        $this->object = new MathBig(['213922337203685477580711,12', '119223372036854775807.3222']);
        $this->object->setOperation(new OperationSum());
        $this->assertEquals('214041560575722332356518.4422', $this->object->process());
    }
    
    /**
     * @covers KirtuZ\MathBig\MathBig::process
     */
    public function testProcessInt()
    {
        $this->object = new MathBig(['213922337203685477580711', '119223372036854775807']);
        $this->object->setOperation(new OperationSum());
        $this->assertEquals('214041560575722332356518', $this->object->process());
    }

    /**
     * @covers KirtuZ\MathBig\MathBig::setOperation
     */
    public function testSetOperationSum()
    {
        $this->object = new MathBig([]);
        $this->object->setOperation(new OperationSum());
        $this->assertTrue($this->object->getOperation() instanceof OperationInterface);
    }

}
