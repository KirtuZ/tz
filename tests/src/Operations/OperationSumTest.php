<?php

namespace KirtuZ\MathBig\Operations;

require_once __DIR__ . '/../../../src/autoload.php';

use PHPUnit\Framework\TestCase;

class OperationSumTest extends TestCase
{

    /**
     * @var OperationSum
     */
    protected $object;

    protected function setUp()
    {
        $this->object = new OperationSum();
    }

    protected function tearDown()
    {
        $this->object = null;
    }

    /**
     * @covers KirtuZ\MathBig\Operations\OperationSum::setOperands
     */
    public function testSetOperands()
    {
        $this->object->setOperands(['213922337203685477580711,12', '119223372036854775807.3222']);
        $this->assertTrue(
            $this->object->a === '213922337203685477580711.12' && $this->object->b === '119223372036854775807.3222'
        );
    }
    
    /**
     * @covers KirtuZ\MathBig\Operations\OperationSum::setOperands
     */
    public function testSetOperandsDelimiter()
    {
        $this->object->setOperands(['213922337203685477580711,12', '119223372036854775807.3222']);
        $this->assertTrue(strpos($this->object->a, '.') !== false);
    }
    
    /**
     * @covers KirtuZ\MathBig\Operations\OperationSum::setOperands
     * @expectedException Exception
     */
    public function testSetOperandsNegative()
    {
        $this->object->setOperands(['-213922337203685477580711.12', '119223372036854775807.3222']);
    }

    /**
     * @covers KirtuZ\MathBig\Operations\OperationSum::math
     */
    public function testMathFloat()
    {
        $this->object->setOperands(['213922337203685477580711,12', '119223372036854775807.3222']);
        $this->assertEquals($this->object->math(), '214041560575722332356518.4422');
    }
    
    /**
     * @covers KirtuZ\MathBig\Operations\OperationSum::math
     */
    public function testMathInt()
    {
        $this->object->setOperands(['213922337203685477580711', '119223372036854775807']);
        $this->assertEquals($this->object->math(), '214041560575722332356518');
    }
}
