<?php

namespace KirtuZ\MathBig;

require_once __DIR__ . '/../../../src/autoload.php';

use PHPUnit\Framework\TestCase;
use KirtuZ\MathBig\Traits\OperandsTrait;

class OperandsTraitTest extends TestCase
{
    protected $object;

    protected function setUp()
    {
        $this->object = new class {
            use OperandsTrait; 
            
        };
    }

    protected function tearDown()
    {
        $this->object = null;
    }

    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::isFloat
     */
    public function testIsFloatTrue() {
        $this->assertTrue($this->object->isFloat('214041560575722332356518.4422'));
    }
    
    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::isFloat
     */
    public function testIsFloatFalse() {
        $this->assertFalse($this->object->isFloat('214041560575722332356518'));
    }
    
    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::calcMaxLength
     */
    public function testCalcMaxLengthFloat()
    {
        $this->object->calcMaxLength('214041560575722332356518.4422');
        $this->assertTrue($this->object->maxIntLength == 24 && $this->object->maxFractionalLength == 4);
    }
    
    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::calcMaxLength
     */
    public function testCalcMaxLengthInt()
    {
        $this->object->calcMaxLength('214041560575722332356518');
        $this->assertEquals($this->object->maxIntLength, 24);
    }
    
    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::padAsMax
     */
    public function testPadAsMaxFloat()
    {
        $this->object->calcMaxLength('214041560575722332356518.4422');
        $this->assertEquals($this->object->padAsMax('1560575722332356518.44'), '000001560575722332356518.4400');
    }
    
    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::padAsMax
     */
    public function testPadAsMaxInt()
    {
        $this->object->calcMaxLength('214041560575722332356518');
        $this->assertEquals($this->object->padAsMax('1560575722332356518'), '000001560575722332356518');
    }
    
    /**
     * @covers KirtuZ\MathBig\Traits\OperandsTrait::padAsMax
     */
    public function testPadAsMaxIntAsFloat()
    {
        $this->object->calcMaxLength('214041560575722332356518.4422');
        $this->assertEquals($this->object->padAsMax('1560575722332356518'), '000001560575722332356518.0000');
    }

}