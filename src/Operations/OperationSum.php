<?php

namespace KirtuZ\MathBig\Operations;

use KirtuZ\MathBig\Traits\OperandsTrait;

/**
 * Класс операции "сумма", операция бинарная - подразумевает работу с двумя операндами.<br/>
 * Любая операция сложения производится над положительными числами.
 *
 * @author KirtuZ
 */
class OperationSum implements OperationInterface
{
    use OperandsTrait;
    
    public $a;
    public $b;

    public function setOperands(array $operands = []): void
    {
        $prepared_operands = array_map(function($operand) {
            if ($operand < 0) {
                throw new \Exception("Передано отрицательное число. Используйте вычитание.");
            }
            
            if (strpos($operand, ',') !== false) {
                $operand = str_replace(',', $this->delimiter, $operand);
            }
            
            $this->calcMaxLength($operand);

            return $operand;
        }, $operands);

        $this->a = (string) $prepared_operands[0];
        $this->b = (string) $prepared_operands[1];
    }

    public function math(): string
    {
        $a = $this->padAsMax($this->a);
        $b = $this->padAsMax($this->b);

        $result = '';

        $temp = 0;
        for ($i = ($this->maxIntLength + $this->maxFractionalLength); $i >= 0; $i--) {
            if ($a[$i] == $this->delimiter) {
                $result = $this->delimiter . $result;
                continue;
            }

            $subA = (int) $a[$i];
            $subB = (int) $b[$i];

            $subSum = $subA + $subB + $temp;

            $temp = 0;

            if ($subSum > 9 && $i != 0) {
                $temp = 1;
                $subSum = $subSum % 10;
            }

            $result = $subSum . $result;
        }

        return $result;
    }

}
