<?php

namespace KirtuZ\MathBig\Operations;

/**
 * @author KirtuZ
 */
interface OperationInterface
{
    /** Производит операцию над операндами
     * 
     * @param array $operands Операнды
     * @return string Результат операции
     */
    public function setOperands(array $operands = []): void;
    public function math(): string;
}
