<?php

namespace KirtuZ\MathBig;

require_once __DIR__ . '/autoload.php';

use KirtuZ\MathBig\Operations\OperationInterface;

class MathBig
{
    /**
     * @var Operation $operation;
     */
    private $operation;

    /**
     * @var array $operation;
     */
    private $operands = [];

    /** Калькулятор больших чисел, всегда будет представлять числа строками
     * 
     * @param array $operands
     */
    public function __construct(array $operands = [])
    {
        $this->operands = $operands;
    }

    /** Устанавливает тип операции
     * 
     * @param OperationInterface $operation
     */
    public function setOperation(OperationInterface $operation): void
    {
        $this->operation = $operation;
    }

    /** Отдает тип операции. (нужен для тестов)
     * 
     * @param OperationInterface $operation
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /** Производит операцию и возвращает результат в виде строки
     * 
     * @return string
     */
    public function process(): string
    {
        $this->operation->setOperands($this->operands);
        
        return $this->operation->math();
    }

}
