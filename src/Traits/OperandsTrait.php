<?php

namespace KirtuZ\MathBig\Traits;

/** Трейт для повторного использования методов операндов.
 *
 * @author KirtuZ
 */
trait OperandsTrait
{

    public $maxIntLength = 0;
    public $maxFractionalLength = -1;
    public $delimiter = '.';

    public function isFloat(string $num): bool
    {
        return strpos($num, $this->delimiter) !== false;
    }

    public function calcMaxLength(string $num): void
    {
        if ($this->isFloat($num)) {
            $parts = explode($this->delimiter, $num);

            $num = $parts[0];

            $fractionalLength = strlen($parts[1]);

            if ($this->maxFractionalLength < $fractionalLength) {
                $this->maxFractionalLength = $fractionalLength;
            }
        }

        $intLength = strlen($num);
        $this->maxIntLength = $this->maxIntLength < $intLength ? $intLength : $this->maxIntLength;
    }

    public function padAsMax(string $num): string
    {
        $fractional = false;
        if ($this->isFloat($num)) {
            $parts = explode($this->delimiter, $num);

            $num = $parts[0];
            $fractional = str_pad($parts[1], $this->maxFractionalLength, '0', STR_PAD_RIGHT);
        } elseif ($this->maxFractionalLength > 0) {
            $fractional = str_pad('', $this->maxFractionalLength, '0', STR_PAD_RIGHT);
        }

        $num = str_pad($num, $this->maxIntLength, '0', STR_PAD_LEFT);

        if ($fractional) {
            return implode($this->delimiter, [$num, $fractional]);
        }

        return $num;
    }

}
